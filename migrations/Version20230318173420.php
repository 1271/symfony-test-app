<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230318173420 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_properties (id UUID NOT NULL, key VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_14A46EEC8A90ABA9 ON product_properties (key)');
        $this->addSql('COMMENT ON COLUMN product_properties.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE product_sections (id UUID NOT NULL, title VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN product_sections.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE products (id UUID NOT NULL, section_id UUID DEFAULT NULL, title VARCHAR(255) NOT NULL, cost INT NOT NULL, is_deleted BOOLEAN NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B3BA5A5AD823E37A ON products (section_id)');
        $this->addSql('COMMENT ON COLUMN products.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products.section_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE product_product_property (product_id UUID NOT NULL, product_property_id UUID NOT NULL, PRIMARY KEY(product_id, product_property_id))');
        $this->addSql('CREATE INDEX IDX_CECE12B54584665A ON product_product_property (product_id)');
        $this->addSql('CREATE INDEX IDX_CECE12B5F8BD8DF3 ON product_product_property (product_property_id)');
        $this->addSql('COMMENT ON COLUMN product_product_property.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN product_product_property.product_property_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5AD823E37A FOREIGN KEY (section_id) REFERENCES product_sections (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_product_property ADD CONSTRAINT FK_CECE12B54584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_product_property ADD CONSTRAINT FK_CECE12B5F8BD8DF3 FOREIGN KEY (product_property_id) REFERENCES product_properties (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE products DROP CONSTRAINT FK_B3BA5A5AD823E37A');
        $this->addSql('ALTER TABLE product_product_property DROP CONSTRAINT FK_CECE12B54584665A');
        $this->addSql('ALTER TABLE product_product_property DROP CONSTRAINT FK_CECE12B5F8BD8DF3');
        $this->addSql('DROP TABLE product_properties');
        $this->addSql('DROP TABLE product_sections');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE product_product_property');
    }
}
