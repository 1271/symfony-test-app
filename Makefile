DC := docker-compose
EXEC := $(DC) exec
PHP := $(EXEC) app
CONSOLE := $(PHP) app bin/console


all: docker-up docker-in

docker-up:
	$(DC) up -d

docker-down:
	-$(DC) down

docker-start:
	$(DC) start

docker-stop:
	-$(DC) stop

docker-build:
	$(DC) up --build -d
	$(PHP) bin/composer install

docker-in:
	$(PHP) sh

# aliases
du: docker-up
dd: docker-down
ds: docker-start
db: docker-build
di: docker-in
