<?php

declare(strict_types=1);

namespace App\Presentation\Product\Views;

use App\Domain\Product\ProductProperty;
use Symfony\Component\Uid\Uuid;

final class ProductPropertyView
{
    public Uuid $id;

    /** Ключ параметра */
    public string $key;

    /** Значение параметра */
    public string $value;

    public function __construct(ProductProperty $property)
    {
        $this->id = $property->getId();
        $this->key = $property->getKey();
        $this->value = $property->getValue();
    }
}
