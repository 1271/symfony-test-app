<?php

declare(strict_types=1);

namespace App\Presentation\Product\Views;

// можно было бы возвращать сущности напрямую пользователю, но я считаю это дурным тоном, поэтому так
use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use Symfony\Component\Uid\Uuid;

final class ProductView
{
    public Uuid $id;

    public string $title;

    public int $cost;

    // не уверен, что тут стоит возвращать раздел, т.к. он будет по сути дублироваться в товарах. Лучше маппить это на фронте
    public Uuid $sectionId;

    public array $properties;

    public function __construct(Product $product)
    {
        $this->id = $product->getId();
        $this->title = $product->getTitle();
        $this->cost = $product->getCost();
        $this->sectionId = $product->getSection()->getId();

        $this->properties = array_map(
            static fn(ProductProperty $property) => new ProductPropertyView($property),
            $product->getProperties(),
        );
    }
}
