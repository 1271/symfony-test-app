<?php

declare(strict_types=1);

namespace App\Presentation\Product;

use App\Application\Product\CreateProduct\CreateProductCommand;
use App\Application\Product\CreateProduct\CreateProductHandler;
use App\Application\Product\DeleteProduct\DeleteProductHandler;
use App\Application\Product\GetProductsQuery\GetProductsHandler;
use App\Application\Product\GetProductsQuery\GetProductsLimitQuery;
use App\Application\Product\RiseProduct\RiseProductHandler;
use App\Domain\Product\Product;
use App\Domain\Product\Repository\ProductRepositoryInterface;
use App\Presentation\Common\PaginatedView;
use App\Presentation\Product\Views\ProductView;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

// todo: т.к. в задании небыло ни слова о том, что присутсвуют какие-то пользователи, поэтому нет никакох проверок на доступы
class ProductController
{
    /**
     * Создает товар
     * @return ProductView
     */
    #[Route("/products", methods: ["POST"])]
    public function createProduct(CreateProductHandler $handler, CreateProductCommand $command)
    {
        // тут можно опустить отправку резуольтата, но зачастую всё-таки возвращают
        return new ProductView($handler($command));
    }

    /**
     * Вернет несколько товаров
     *
     * @return PaginatedView<ProductView>
     */
    #[Route("/products", methods: ["GET"])]
    public function getAllProducts(GetProductsHandler $handler, GetProductsLimitQuery $query)
    {
        $handlerResponse = $handler($query);

        return new PaginatedView(
            data: array_map(static fn(Product $product) => new ProductView($product), $handlerResponse['data']),
            total: $handlerResponse['total'],
            limit: $query->limit,
            offset: $query->offset,
        );
    }

    /**
     * Вернет 1 товар.
     * Можно было вынести в в отдельную квери, как выше, но суть не изменится
     *
     * @return ProductView
     */
    #[Route("/products/{productId}", requirements: ["productId" => "%routing.uuid%"], methods: ["GET"])]
    public function getOneProduct(ProductRepositoryInterface $repository, Uuid $productId)
    {
        return new ProductView($repository->getProduct($productId));
    }

    #[Route("/products/{productId}/delete", requirements: ["productId" => "%routing.uuid%"], methods: ["DELETE"])]
    public function deleteProduct(DeleteProductHandler $handler, Uuid $productId): void
    {
        $handler($productId);
    }

    #[Route("/products/{productId}/rise", requirements: ["productId" => "%routing.uuid%"], methods: ["POST"])]
    public function riseProduct(RiseProductHandler $handler, Uuid $productId): void
    {
        $handler($productId);
    }
}
