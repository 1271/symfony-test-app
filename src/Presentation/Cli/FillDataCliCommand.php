<?php

declare(strict_types=1);

namespace App\Presentation\Cli;

use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use App\Domain\Product\ProductSection;
use App\Domain\Product\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Uid\Uuid;

class FillDataCliCommand extends Command
{
    private array $properties = [];

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProductRepositoryInterface $productRepository,
        string $name = 'app:fill-data',
    ) {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->entityManager->wrapInTransaction(fn() => $this->fillData());

            $output->writeln("Success!");

            return 0;
        }
        catch (\Throwable $e) {
            $output->writeln(sprintf(
                "Exception: %s", $e->getMessage()
            ));

            return 1;
        }
    }

    private function fillData(): void
    {
        $section = new ProductSection(sprintf('Section - %s', Uuid::v4()->toRfc4122()));

        $this->prepareProperties();

        foreach (range(0, 19) as $_) {
            $product = $this->buildProduct($section);
            $this->productRepository->storeProduct($product);
        }
    }

    private function buildProduct(ProductSection $section): Product
    {
        $product = new Product(
            $section,
            sprintf('Product - %s', Uuid::v4()->toRfc4122()),
            random_int(100, 5000),
        );

        foreach ($this->properties as $property) {
            if (random_int(1, 5) > 3) {
                $product->addProperty($property);
            }
        }

        return $product;
    }

    private function prepareProperties(): void
    {
        foreach (range(0, 19) as $_) {
            $this->properties[] = new ProductProperty(
                propertyKey: sprintf('KEY_%s', Uuid::v4()->toRfc4122()),
                propertyValue: sprintf('VALUE_%s', Uuid::v4()->toRfc4122()),
            );
        }
    }
}
