<?php

declare(strict_types=1);

namespace App\Presentation\Common;

/**
 * @template T
 */
class PaginatedView
{
    public function __construct(
        public array $data,
        public int $total,
        public int $limit,
        public int $offset
    ) {
    }
}
