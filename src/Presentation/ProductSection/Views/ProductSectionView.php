<?php

declare(strict_types=1);

namespace App\Presentation\ProductSection\Views;

use App\Domain\Product\ProductSection;
use Symfony\Component\Uid\Uuid;

final class ProductSectionView
{
    public Uuid $id;

    public string $title;

    public function __construct(ProductSection $section)
    {
        $this->id = $section->getId();
        $this->title = $section->getTitle();
    }
}
