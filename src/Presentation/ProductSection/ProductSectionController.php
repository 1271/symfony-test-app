<?php

declare(strict_types=1);

namespace App\Presentation\ProductSection;

use App\Application\Common\OffsetLimitQuery;
use App\Application\ProductSection\CreateProductSection\CreateProductSectionCommand;
use App\Domain\Product\Product;
use App\Domain\Product\ProductSection;
use App\Domain\Product\Repository\ProductSectionRepositoryInterface;
use App\Presentation\Common\PaginatedView;
use App\Presentation\Product\Views\ProductView;
use App\Presentation\ProductSection\Views\ProductSectionView;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

// todo: в задании указано, что существуют разделы, но т.к. они скорее для синтетики, поэтому они тут "как есть"
class ProductSectionController
{
    public function __construct(
        private ProductSectionRepositoryInterface $sectionRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route("/product/sections", methods: ["POST"])]
    public function createSection(CreateProductSectionCommand $command): ProductSectionView
    {
        $section = new ProductSection($command->title);

        $this->entityManager->wrapInTransaction(
            static fn() => $this->sectionRepository->storeProductSection($section)
        );

        return new ProductSectionView($section);
    }

    /**
     * @return PaginatedView<ProductSectionView>
     */
    #[Route("/product/sections", methods: ["GET"])]
    public function getAllSections(OffsetLimitQuery $query)
    {
        $response = $this->sectionRepository->findAllProductSections(offset: $query->offset, limit: $query->limit);

        return new PaginatedView(
            data: array_map(
                static fn(ProductSection $section) => new ProductSectionView($section),
                $response['data'],
            ),
            total: $response['total'],
            limit: $query->limit,
            offset: $query->offset,
        );
    }
}
