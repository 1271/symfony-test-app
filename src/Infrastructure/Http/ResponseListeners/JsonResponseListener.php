<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\ResponseListeners;

use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

final class JsonResponseListener
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    /**
     * @throws JsonException
     */
    public function handle(ViewEvent $event): void
    {
        try {
            $result = $event->getControllerResult();

            if ($result instanceof Response) {
                return;
            }

            $request = $event->getRequest();

            $response = new JsonResponse();

            if (null !== $result) {
                $data = $this->serializer->serialize($result, 'json');
                $response->setJson($data);
                $response->setStatusCode(
                    $request->isMethod('POST') ? Response::HTTP_CREATED : Response::HTTP_OK
                );
            } else {
                $response->setStatusCode(Response::HTTP_NO_CONTENT);
            }

            $event->setResponse($response);
        } catch (Throwable $e) {
            // todo: логирование
            //$this->logger->warning("Response serialization error.\n$e");

            $response = new JsonResponse(json_encode([
                'type' => 'ResponseSerializationError',
                'message' => $e->getMessage(),
            ], JSON_THROW_ON_ERROR), json: true);

            $response->setStatusCode(500);

            $event->setResponse($response);
        }
    }
}
