<?php

declare(strict_types=1);

namespace App\Infrastructure\Serialization\Resolvers;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait RemapErrorsTrait
{
    private function remapValidationErrors(ConstraintViolationListInterface $errors): array
    {
        $result = [];

        foreach ($errors as $key => $error) {
            if ($error instanceof ConstraintViolation) {
                $constraint = $error->getConstraint();

                $result[] = [
                    'key' => $key,
                    'property' => $error->getPropertyPath(),
                    'constraint' => $constraint ? (new \ReflectionObject($constraint))->getShortName() : null,
                    'value' => $error->getInvalidValue(),
                    'message' => $error->getMessage(),
                ];
            } else {
                $result[] = [
                    'key' => $key,
                    'property' => 'undefined',
                    'constraint' => 'undefined',
                    'message' => 'undefined',
                ];
            }
        }

        return $result;
    }
}
