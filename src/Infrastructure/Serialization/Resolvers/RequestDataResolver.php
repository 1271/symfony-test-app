<?php

declare(strict_types=1);

namespace App\Infrastructure\Serialization\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class RequestDataResolver implements ArgumentValueResolverInterface
{
    public function __construct(private DenormalizerInterface $denormalizer)
    {
    }

    /**
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $request->attributes->has($argument->getName());
    }

    /**
     * @return iterable
     */
    public function resolve(Request $request, ArgumentMetadata $argument): mixed
    {
        $name = $argument->getName();
        $type = $argument->getType();

        if (null === $type) {
            throw (new \RuntimeException('Null type exception'));
        }

        $value = $request->attributes->get($name);

        yield $this->deserializeValue($type, $value);
    }

    private function deserializeValue(mixed $type, mixed $value)
    {
        if ($value instanceof \Throwable) {
            // не делал обработку ошибок, поэтому возвращаем результат "как есть"
            return $value;
        }

        // скалярные типы можно не денормализировать, поэтому так (тем более, что денормализатор не совсем корректно работает с ними)
        return match ($type) {
            'string' => (string)$value,
            'int' => null === $value ? null : (int)$value,
            'float' => null === $value ? null : (float)$value,
            'bool' => in_array(strtolower($value), ['true', '1', 'on'], true),
            default => null === $value ? null : $this->denormalizer->denormalize($value, $type),
        };
    }
}
