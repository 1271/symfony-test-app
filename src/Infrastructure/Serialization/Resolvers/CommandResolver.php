<?php

declare(strict_types=1);

namespace App\Infrastructure\Serialization\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommandResolver implements ArgumentValueResolverInterface
{
    use RemapErrorsTrait;

    public function __construct(
        private SerializerInterface $serializer,
        private ValidatorInterface $validator,
        private DenormalizerInterface $denormalizer,
    ) {
    }

    /**
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        $argumentType = $argument->getType();

        // Это просто пример. Именно для этого задания
        return null !== $argumentType
            && str_ends_with($argumentType, 'Command')
            && !$request->isMethod('GET')
            && !$request->isMethod('HEAD');
    }

    /**
     * @return iterable
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $content = $request->getContent();

        if (null === $content) {
            yield null;
        }
        else {
            $type = $argument->getType();

            if ($argument->isVariadic()) {
                yield from $this->serializer->deserialize($content, sprintf('%s[]', $type), 'json');
            } else {
                yield $this->serializer->deserialize($content, $type, 'json');
            }
        }
    }
}
