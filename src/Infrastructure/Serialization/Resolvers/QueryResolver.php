<?php

declare(strict_types=1);

namespace App\Infrastructure\Serialization\Resolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class QueryResolver implements ArgumentValueResolverInterface
{
    use RemapErrorsTrait;
    use ContentDenormalizerTrait;

    public function __construct(private SerializerInterface $serializer)
    {
    }

    /**
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        $argumentType = $argument->getType();

        // Это просто пример. Именно для этого задания
        return null !== $argumentType
            && str_ends_with($argumentType, 'Query')
            && ($request->isMethod('GET') || $request->isMethod('HEAD'));
    }

    /**
     * @return iterable
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        if ($argument->isVariadic()) {
            // то же самое
            yield from $this->deserializeContent($request->query->all(), $argument, $this->serializer);
        } else {
            yield $this->deserializeContent($request->query->all(), $argument, $this->serializer);
        }
    }

}
