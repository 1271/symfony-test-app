<?php

declare(strict_types=1);

namespace App\Infrastructure\Serialization\Resolvers;

use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;

trait ContentDenormalizerTrait
{
    private function deserializeContent(array $data, ArgumentMetadata $argument, SerializerInterface $serializer): ?object
    {
        // тут в данный момент нет поддержки вложеннысти, т.к. для задания этого не требуется
        $type = $argument->getType();

        $classRef = new \ReflectionClass($type);

        $classInstance = $classRef->newInstance();

        foreach ($classRef->getProperties() as $property) {
            $this->checkPropertyAccess($property);

            $caster = $this->getPropertyValueCaster($property, $serializer);

            if ($property->hasDefaultValue()) {
                $value = $property->getDefaultValue();
            }

            if (array_key_exists($property->name, $data)) {
                $value = $data[$property->name];
            }

            if (!isset($value) && $property->getType()?->allowsNull()) {
                $value = null;
            } elseif (!isset($value)) {
                throw new \RuntimeException(sprintf('Query argument "%s" required', $property->name));
            }

            $classInstance->{$property->getName()} = $caster($value);
        }

        return $classInstance;
    }

    private function getPropertyValueCaster(\ReflectionProperty $property, SerializerInterface $serializer): callable
    {
        $typeRef = $property->getType();

        if ($typeRef instanceof \ReflectionNamedType) {
            $type = $typeRef->getName();
        } else {
            throw new \RuntimeException('Unsupported type');
        }

        $castBoolFn = static fn($value) => in_array(strtolower($value), ['true', '1', 'on'], true);

        return match ($type) {
            'string' => static fn($value) => null === $value ? null : (string)$value,
            'int' => static fn($value) => null === $value ? null : (int)$value,
            'float' => static fn($value) =>  null === $value ? null : (float)$value,
            'bool' => static fn($value) => is_bool($value) ? $value : $castBoolFn($value),
            default => static fn($value) => null === $value ? null : $serializer->denormalize($value, $type),
        };
    }

    private function checkPropertyAccess(\ReflectionProperty $property): void
    {
        if ($property->isReadOnly() || !$property->isPublic() || $property->isStatic()) {
            throw new \RuntimeException('Check property visibility');
        }
    }
}
