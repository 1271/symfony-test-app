<?php declare(strict_types=1);

namespace App\Infrastructure\DependencyInjection;


use ReflectionClass;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

// автоматический маппер репозиториев
final class RepositoryCompiler implements CompilerPassInterface
{
    public const REPOSITORY_TAG = 'doctrine.repository';
    private const REPOSITORY_POSTFIX = 'RepositoryInterface';

    public function process(ContainerBuilder $container): void
    {
        $servicesIds = array_keys($container->findTaggedServiceIds(self::REPOSITORY_TAG));

        foreach ($servicesIds as $id) {
            $class = $container->getDefinition($id)->getClass();

            if (null === $class) {
                continue;
            }

            $implementation = new ReflectionClass($class);

            foreach ($implementation->getInterfaces() as $interface) {
                if (str_ends_with($interface->name, self::REPOSITORY_POSTFIX)) {
                    $container->setAlias($interface->name, $implementation->name);
                }
            }
        }
    }
}
