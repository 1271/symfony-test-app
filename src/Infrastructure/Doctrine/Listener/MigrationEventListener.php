<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Listener;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;
use Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;

// исключает постоянное добавление в миграции строки:
// $this->addSql('CREATE SCHEMA public');
class MigrationEventListener
{
    private ?AbstractPlatform $platform = null;

    public function __construct(Connection $connection)
    {
        try {
            $this->platform = $connection->getDatabasePlatform();
        } catch (Exception) {
        }
    }

    public function postGenerateSchema(GenerateSchemaEventArgs $eventArgs): void
    {
        if (null === $this->platform) {
           return;
        }

        try {
            $schema = $eventArgs->getSchema();

            $defaultSchema = $this->platform->getDefaultSchemaName();

            if (!$schema->hasNamespace($defaultSchema)) {
                $schema->createNamespace($defaultSchema);
            }
        } catch (Exception) {
        }
    }
}
