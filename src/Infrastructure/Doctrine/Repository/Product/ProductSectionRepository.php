<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository\Product;

use App\Infrastructure\Doctrine\Repository\AbstractRepository;
use App\Domain\Product\ProductSection;
use App\Domain\Product\Repository\ProductSectionRepositoryInterface;
use Symfony\Component\Uid\Uuid;

class ProductSectionRepository extends AbstractRepository implements ProductSectionRepositoryInterface
{
    public function getProductSection(Uuid $sectionId): ProductSection
    {
        return $this->getById(ProductSection::class, $sectionId);
    }

    public function storeProductSection(ProductSection $product): void
    {
        $this->store($product);
    }

    public function findAllProductSections(int $offset, int $limit): array
    {
        $query = $this->createBuilder()
            ->from(ProductSection::class, 's')
            ->select('s')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $this->getWithTotal($query);
    }
}
