<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository\Product;

use App\Infrastructure\Doctrine\Repository\AbstractRepository;
use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use App\Domain\Product\Repository\ProductRepositoryInterface;
use Symfony\Component\Uid\Uuid;

class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{
    public function getProduct(Uuid $productId): Product
    {
        return $this->getById(Product::class, $productId);
    }

    public function storeProduct(Product $product): void
    {
        $this->store($product);
    }

    public function findAllProducts(int $offset, int $limit): array
    {
        $queryBuilder = $this->createBuilder()
            ->from(Product::class, 'p')
            ->select('p')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $this->getWithTotal($queryBuilder);
    }

    public function findAllProductsBySectionId(int $offset, int $limit, Uuid $sectionId): array
    {
        $queryBuilder = $this->createBuilder()
            ->from(Product::class, 'p')
            ->select('p')
            ->where('p.sectionId = :sectionId')
            ->setParameter('sectionId', $sectionId->toRfc4122())
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $this->getWithTotal($queryBuilder);
    }

    public function findAllProductsByPropertyId(int $offset, int $limit, Uuid $propertyId): array
    {
        $queryBuilder = $this->createBuilder()
            ->from(Product::class, 'p')
            ->select('p')
            ->innerJoin(ProductProperty::class, 'pp')
            ->where('pp.id = :property')
            ->setParameter('property', $propertyId->toRfc4122())
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $this->getWithTotal($queryBuilder);
    }
}
