<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository\Product;

use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use App\Domain\Product\Repository\ProductPropertyRepositoryInterface;
use App\Infrastructure\Doctrine\Repository\AbstractRepository;

class ProductPropertyRepository extends AbstractRepository implements ProductPropertyRepositoryInterface
{
    public function storeProductProperty(ProductProperty $productProperty): void
    {
        $this->store($productProperty);
    }

    public function findAllProductProperties(int $offset, int $limit): array
    {
        $queryBuilder = $this->createBuilder()
            ->from(ProductProperty::class, 'pp')
            ->select('pp')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $this->getWithTotal($queryBuilder);
    }

    public function findAllProductPropertiesWithFilter(array $ids): array
    {
        return $this->createBuilder()
            ->from(ProductProperty::class, 'pp')
            ->select('pp')
            ->where('pp.id IN(:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }
}
