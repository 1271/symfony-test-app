<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\EntityInterface;
use Doctrine\DBAL\Query\QueryBuilder as DbalBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query\Expr\Select;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;
use Symfony\Component\Uid\Uuid;

abstract class AbstractRepository
{
    private ManagerRegistry $managerRegistry;

    /**
     * Entity manager может иметь состояние open/closed.
     * Если em закрыли извне (например какой-то долгоживущий сервис или команда), его надо будет открыть заново, т.к. ссылка уже протухнет к тому моменту
     *
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        $manager = $this->managerRegistry->getManager();

        if (!$manager instanceof EntityManagerInterface) {
            $type = get_debug_type($manager);
            throw new \RuntimeException("Repository expected entity manager to be instance of EntityManagerInterface, got '{$type}'");
        }

        return $manager;
    }

    public function setManager(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    protected function createBuilder(): QueryBuilder
    {
        return $this->getEntityManager()->createQueryBuilder();
    }

    protected function createDbalBuilder(): DbalBuilder
    {
        return $this->getEntityManager()->getConnection()->createQueryBuilder();
    }

    protected function store(EntityInterface $entity)
    {
        if ($this->getEntityManager()->contains($entity)) {
            throw new RuntimeException('Entity already added to store');
        }

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * @psalm-template TEntity
     *
     * @param class-string<TEntity> $entityClass
     * @param Uuid $id
     *
     * @return TEntity
     */
    protected function getById(string $entityClass, Uuid $id): object
    {
        $foundId = $id->toRfc4122();

        /** @var null|TEntity */
        $found = $this->getEntityManager()->find($entityClass, $foundId);

        if (null === $found) {
            throw EntityNotFoundException::fromClassNameAndIdentifier($entityClass, [$foundId]);
        }

        return $found;
    }

    /**
     * @return array {
     *     'total' => "mixed",
     *     'data' => "mixed"
     * }
     */
    protected function getWithTotal(QueryBuilder $queryBuilder): array {
        $totalQueryBuilder = clone $queryBuilder;

        $total = (int)$totalQueryBuilder
            ->select(sprintf("count(%s.id)", current($queryBuilder->getRootAliases())))
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();

        return [
            'data' => $queryBuilder->getQuery()->getResult(),
            'total' => $total,
        ];
    }
}
