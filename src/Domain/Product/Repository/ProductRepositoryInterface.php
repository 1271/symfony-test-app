<?php

declare(strict_types=1);

namespace App\Domain\Product\Repository;

use App\Domain\Product\Product;
use Symfony\Component\Uid\Uuid;

interface ProductRepositoryInterface
{
    public function getProduct(Uuid $productId): Product;

    public function storeProduct(Product $product): void;

    /**
     * todo: для findAll* в идеале нужно 2 DTO.
     *  Один на вход с параментами и фильтрами, другой как результат (туда же total).
     *  Однако для простоты сейчас сделано так.
     *
     * @return array {
     *     "data": list<Product>,
     *     "total": int,
     * }
     */
    public function findAllProducts(int $offset, int $limit): array;

    /**
     * @return array {
     *     "data": list<Product>,
     *     "total": int,
     * }
     */
    public function findAllProductsBySectionId(int $offset, int $limit, Uuid $sectionId): array;

    /**
     * @return array {
     *     "data": list<Product>,
     *     "total": int,
     * }
     */
    public function findAllProductsByPropertyId(int $offset, int $limit, Uuid $propertyId): array;
}
