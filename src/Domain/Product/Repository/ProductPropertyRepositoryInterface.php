<?php

declare(strict_types=1);

namespace App\Domain\Product\Repository;

use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use Symfony\Component\Uid\Uuid;

interface ProductPropertyRepositoryInterface
{
    public function storeProductProperty(ProductProperty $productProperty): void;

    /**
     * @return array {
     *     "data": list<ProductProperty>,
     *     "total": int,
     * }
     */
    public function findAllProductProperties(int $offset, int $limit): array;

    /**
     * @return list<ProductProperty>
     */
    public function findAllProductPropertiesWithFilter(array $ids): array;
}
