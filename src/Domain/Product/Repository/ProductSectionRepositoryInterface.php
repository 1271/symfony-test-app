<?php

declare(strict_types=1);

namespace App\Domain\Product\Repository;

use App\Domain\Product\ProductSection;
use Symfony\Component\Uid\Uuid;

interface ProductSectionRepositoryInterface
{
    public function getProductSection(Uuid $sectionId): ProductSection;

    public function storeProductSection(ProductSection $product): void;

    /**
     * // пояснения смотри в ProductRepositoryInterface
     *
     * @return array{
     *     "data": list<ProductSection>,
     *     "total": int,
     * }
     */
    public function findAllProductSections(int $offset, int $limit): array;
}
