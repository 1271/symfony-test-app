<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ProductSection extends BaseEntity
{
    private string $title;

    private Collection $products;

    public function __construct(string $title)
    {
        parent::__construct();

        $this->products = new ArrayCollection();
        $this->title = $title;
    }

    /**
     * @return list<Product>
     */
    public function getProducts(): array
    {
        return $this->products->getValues();
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
