<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\BaseEntity;

// Не совсем понял, что должны представлять собой эти "свойства", поэтому так
class ProductProperty extends BaseEntity
{
    private string $key;

    private string $value;

    public function __construct(string $propertyKey, string $propertyValue)
    {
        parent::__construct();

        $this->key = $propertyKey;
        $this->value = $propertyValue;
    }

    /// region key

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /// region key

    /// region value

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /// endregion value
}
