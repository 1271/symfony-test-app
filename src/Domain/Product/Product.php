<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Product extends BaseEntity
{
    private ProductSection $section;

    private string $title;

    private int $cost;

    private Collection $properties;

    // комментариев по поводу удаления небыло, но я предпочитаю никогда не удалять подобные данные, так что будет "мягкое" удаление.
    private bool $isDeleted = false;

    public function __construct(ProductSection $section, string $title, int $cost)
    {
        parent::__construct();

        $this->cost = $cost;
        $this->section = $section;
        $this->title = $title;

        $this->properties = new ArrayCollection();
    }

    /// region common getters

    public function getSection(): ProductSection
    {
        return $this->section;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCost(): int
    {
        return $this->cost;
    }

    /// endregion common getters

    /// region properties

    /**
     * @return list<ProductProperty>
     */
    public function getProperties(): array
    {
        return $this->properties->getValues();
    }

    public function addProperty(ProductProperty $property): void
    {
        /// в качестве простого пример прокатит, но лучше сделать проверку в Application, т.к. если тут будет очень много свойств, всё умрёт.
        if (!$this->properties->contains($property)) {
            $this->properties->add($property);
        }
    }

    public function removeProperty(ProductProperty $property): void
    {
        /// То же самое
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);
        }
    }

    /// endregion properties

    /// region delete
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    public function delete(): void
    {
        $this->isDeleted = true;
    }

    public function rise(): void
    {
        $this->isDeleted = false;
    }

    /// endregion delete
}
