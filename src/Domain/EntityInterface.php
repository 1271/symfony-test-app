<?php

declare(strict_types=1);

namespace App\Domain;

use Symfony\Component\Uid\Uuid;

interface EntityInterface
{
    public function getId(): Uuid;
    public function getIdAsString(): string;
}
