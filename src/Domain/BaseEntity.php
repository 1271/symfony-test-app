<?php

declare(strict_types=1);

namespace App\Domain;

use Symfony\Component\Uid\Uuid;

abstract class BaseEntity implements EntityInterface
{
    protected Uuid $id;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdAsString(): string
    {
        return $this->id->toRfc4122();
    }
}
