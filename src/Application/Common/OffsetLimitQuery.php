<?php

declare(strict_types=1);

namespace App\Application\Common;

use Symfony\Component\Validator\Constraints as Assert;

class OffsetLimitQuery
{
    #[Assert\Range(min: 0, max: 100)]
    public int $limit = 50;

    #[Assert\Range(min: 0, max: PHP_INT_MAX)]
    public int $offset = 0;
}
