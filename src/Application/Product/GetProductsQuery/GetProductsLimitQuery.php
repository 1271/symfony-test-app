<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductsQuery;

use App\Application\Common\OffsetLimitQuery;

class GetProductsLimitQuery extends OffsetLimitQuery
{
    // todo: дополнительные фильтры для критерии
}
