<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductsQuery;

use App\Domain\Product\Repository\ProductRepositoryInterface;

class GetProductsHandler
{
    public function __construct(private ProductRepositoryInterface $productRepository)
    {
    }

    /**
     * @return array {
     *     "data": list<Product>,
     *     "total": int,
     * }
     */
    public function __invoke(GetProductsLimitQuery $query): array
    {
        return $this->productRepository->findAllProducts(offset: $query->offset, limit: $query->limit);
    }
}
