<?php

declare(strict_types=1);

namespace App\Application\Product\CreateProduct;

use App\Domain\Product\Product;
use App\Domain\Product\ProductProperty;
use App\Domain\Product\Repository\ProductPropertyRepositoryInterface;
use App\Domain\Product\Repository\ProductRepositoryInterface;
use App\Domain\Product\Repository\ProductSectionRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

// todo: можно и желательно использовать readonly, но у меня шторм 2021, поэтому не умеет в фичи php8.2, уж соре
final class CreateProductHandler
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private ProductSectionRepositoryInterface $sectionRepository,
        private ProductPropertyRepositoryInterface $productPropertyRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(CreateProductCommand $command): Product
    {
        $section = $this->sectionRepository->getProductSection($command->sectionId);
        $product = new Product($section, $command->title, $command->cost);
        foreach ($this->findProductProperties($command->properties) as $property) {
            $product->addProperty($property);
        }
        // Ещё лучше вынести это на уровень инфраструктуры и отслеживать через эвенты, но опять же - такой задачи не стояло.
        // К тому же для тестового это перебор. Поэтому так
        $this->entityManager->wrapInTransaction(
            fn() => $this->productRepository->storeProduct($product)
        );

        return $product;
    }

    /**
     * @param list<Uuid> $properties
     *
     * @return list<ProductProperty>
     */
    private function findProductProperties(array $properties): array
    {
        // todo: нужно проверить, что нашли всё, что попросили
        return $this->productPropertyRepository->findAllProductPropertiesWithFilter($properties);
    }
}
