<?php

declare(strict_types=1);

namespace App\Application\Product\CreateProduct;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;


final class CreateProductCommand
{
    /**
     * Название товара
     */
    #[Assert\Type("string")]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public string $title;

    /**
     * Цена товара
     */
    #[Assert\Type("integer")]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public int $cost;

    /**
     * Раздел
     */
    public Uuid $sectionId;

    /**
     * Список свойств товара.
     *
     * // тут можно использовать key вместо id, но такая задача не по сути стояла
     *
     * @var Uuid[] $properties
     */
    public array $properties = [];
}
