<?php

declare(strict_types=1);

namespace App\Application\Product\RiseProduct;

use App\Domain\Product\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

class RiseProductHandler
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(Uuid $productId)
    {
        $this->entityManager->wrapInTransaction(fn() => $this->productRepository->getProduct($productId)->rise());
    }
}
