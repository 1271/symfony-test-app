<?php

declare(strict_types=1);

namespace App\Application\ProductSection\CreateProductSection;

use Symfony\Component\Validator\Constraints\NotBlank;

class CreateProductSectionCommand
{
    #[NotBlank]
    public string $title;
}
